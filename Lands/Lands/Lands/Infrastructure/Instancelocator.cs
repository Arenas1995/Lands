﻿namespace Lands.Infrastructure
{
    using ViewModels;

    public class Instancelocator
    {
        #region Properties
        public MainViewModel Main
        {
            get;
            set;
        }
        #endregion

        #region Contructors
        public Instancelocator()
        {
            this.Main = new MainViewModel();
        }
        #endregion
    }
}
